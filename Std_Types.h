#ifndef STD_TYPES_H_
#define STD_TYPES_H_

#include "Compiler.h"
#include "Platform_Types.h"

/* type definitions mixed with possible value meanings */
typedef uint8 Std_ReturnType;

#define E_OK 0x00u
#define E_NOT_OK 0x01u

#define STD_HIGH 0x01u /* Physical state 5V or 3.3V */
#define STD_LOW 0x00u /* Physical state 0V */

#define STD_ON 0x01u
#define STD_OFF 0x00u

typedef struct {
    uint16 vendorID;
    uint16 moduleID;
    uint8 sw_major_version;
    uint8 sw_minor_version;
    uint8 sw_patch_version;
}Std_VersionInfoType;









#endif
