# README #

This README would normally document whatever steps are necessary to get your application up and running.

#### Note:####
For a more comprehensive documentation check: documentation/doxygen/html/index.html

### What is this repository for? ###

* This is a public repository which includes an implementation for the DIO Module of the AUTOSAR r4.3.1 on AVR ATMega328p which is included - by default - within the Arduino Uno board. This work is considered as an assignment/practice for the course of "Introduction to AUTOSAR" provided by Ramp Up Academy.

### How do I get set up? ###

* This code can be compiled using (main.c -> main.hex):

```
- avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o <file-1>.o <file-1>.c
- avr-gcc -Os -DF_CPU=16000000UL -mmcu=atmega328p -c -o <file-n>.o <file-n>.c
- avr-gcc -mmcu=atmega328p <file-1>.o <file-2>.o ... <file-n>.o -o <file>
- avr-objcopy -O ihex -R .eeprom <file> <file>.hex
```

* Then, it can be burned using:

```
- avrdude -F -V -c arduino -p ATMEGA328P -P <com-port> -b 115200 -U flash:w:<file>.hex
```

* Note: These commands are prepared for and compitable with the Arduino Uno board.
