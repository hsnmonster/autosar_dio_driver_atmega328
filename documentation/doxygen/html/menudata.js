var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"Related Pages",url:"pages.html"},
{text:"Modules",url:"modules.html"},
{text:"Data Structures",url:"annotated.html",children:[
{text:"Data Structures",url:"annotated.html"},
{text:"Data Structure Index",url:"classes.html"},
{text:"Data Fields",url:"functions.html",children:[
{text:"All",url:"functions.html"},
{text:"Variables",url:"functions_vars.html"}]}]},
{text:"Files",url:"files.html",children:[
{text:"File List",url:"files.html"},
{text:"Globals",url:"globals.html",children:[
{text:"All",url:"globals.html",children:[
{text:"d",url:"globals.html#index_d"},
{text:"e",url:"globals.html#index_e"},
{text:"g",url:"globals.html#index_g"},
{text:"m",url:"globals.html#index_m"},
{text:"s",url:"globals.html#index_s"},
{text:"u",url:"globals.html#index_u"}]},
{text:"Functions",url:"globals_func.html"},
{text:"Variables",url:"globals_vars.html"},
{text:"Typedefs",url:"globals_type.html"},
{text:"Macros",url:"globals_defs.html",children:[
{text:"d",url:"globals_defs.html#index_d"},
{text:"e",url:"globals_defs.html#index_e"},
{text:"g",url:"globals_defs.html#index_g"},
{text:"s",url:"globals_defs.html#index_s"}]}]}]}]}
