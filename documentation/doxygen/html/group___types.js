var group___types =
[
    [ "Dio_ChannelGroupType", "struct_dio___channel_group_type.html", [
      [ "mask", "struct_dio___channel_group_type.html#a07109ee18de664ab3a55b29bf69c4449", null ],
      [ "offset", "struct_dio___channel_group_type.html#af2f806089451be6269e0b22880d71e6e", null ],
      [ "port", "struct_dio___channel_group_type.html#a642b11d039a939147a204c3bee8c450b", null ]
    ] ],
    [ "Dio_ChannelType", "group___types.html#gadf1403b09d6bcfe79c72642393346e3f", null ],
    [ "Dio_LevelType", "group___types.html#ga62c4a3121a6c6076d46164301eee4629", null ],
    [ "Dio_PortLevelType", "group___types.html#ga3a678f208215fe639c2408a1ea685e11", null ],
    [ "Dio_PortType", "group___types.html#ga75f9b1d280fb8b719fce25b7b470ad13", null ]
];