var annotated_dup =
[
    [ "Dio_ChannelGroupType", "struct_dio___channel_group_type.html", "struct_dio___channel_group_type" ],
    [ "Dio_GpioType", "struct_dio___gpio_type.html", "struct_dio___gpio_type" ],
    [ "Dio_PhysicalChannelGroupType", "struct_dio___physical_channel_group_type.html", "struct_dio___physical_channel_group_type" ],
    [ "Dio_PhysicalChannelType", "struct_dio___physical_channel_type.html", "struct_dio___physical_channel_type" ],
    [ "Dio_PhysicalPortType", "struct_dio___physical_port_type.html", "struct_dio___physical_port_type" ],
    [ "Std_VersionInfoType", "struct_std___version_info_type.html", "struct_std___version_info_type" ]
];