/**
 ******************************************************************************
 * @file  Dio_Cfg.h
 * @brief This is the header file (Type Definitions) for the pre-compile 
 *        configurations of the Dio module of AUTOSAR r4.3.1.
 * 
 ****************************************************************************** 
 */

#ifndef DIO_CFG_H_
#define DIO_CFG_H_

#include "Std_Types.h" /* non standard dependency */

/**
 * @addtogroup Configuration
 * @{
 */

/**
 * @addtogroup Configuration_Fields
 * @{
 */

/* configuration fields */
#define DIO_DEV_ERROR_DETECT STD_ON

#define DIO_FLIP_CHANNEL_API STD_ON

#define DIO_VERSION_INFO_API STD_OFF

/**
 * @addtogroup Configurable_Constants
 * @{
 */

/* configurable constants */
#define DIO_END_OF_LIST  (-1u)

/**
 * @}
 */

/**
 * @}
 */

/**
 * @addtogroup ATMega328p_GPIO_PORT_Definitions
 * @{
 */

/* definition of atmega328p gpio ports */
typedef struct {
    volatile uint8 PinRegister; /**< PIN Register */
    volatile uint8 DdrRegister; /**< DDR Register */
    volatile uint8 PortRegister; /**< PORT Register */
}Dio_GpioType;

#define DIO_PORTB_BASE (0x23u)
#define DIO_PORTC_BASE (0x26u)
#define DIO_PORTD_BASE (0x29u)

#define GPIOB ((Dio_GpioType*)DIO_PORTB_BASE)
#define GPIOC ((Dio_GpioType*)DIO_PORTC_BASE)
#define GPIOD ((Dio_GpioType*)DIO_PORTD_BASE)

/**
 * @}
 */

/**
 * @addtogroup Symbolic_Names_Channels_Ports_Groups
 * @{
 */

/* symbolic ids for channels - channel groups - ports */
#define DIO_CHANNEL_ID_ARDUINO_UNO_LED 0u

#define DIO_CHANNEL_GROUP_ID_INPUT_BUTTONS_PTR &(DioChannelGroupConfigData[0].ChannelGroup)

#define DIO_PORT_ID_INPUT_BUTTONS 1u

#define DIO_PORT_ID_7_SEGMENT 0u

/**
 * @}
 */

/**
 * @addtogroup Types_Physical_Channels_Ports_Groups
 * @{
 */

/* physical structure definitions for channel - channel group - port */
typedef struct {
    Dio_ChannelType ChannelId; /**< Numeric ID of a DIO channel */
    Dio_GpioType* GpioPortPtr; /**< Pointer to the physical respective port of the channel */
    uint8 PinId; /**< Pin Number within its respective */
}Dio_PhysicalChannelType;

typedef struct {
    Dio_ChannelGroupType ChannelGroup; /**< The Channel Group, which consists of several adjoining channels within a port */
    Dio_GpioType* GpioPortPtr; /**< Pointer to the physical respective port of the channel */
}Dio_PhysicalChannelGroupType;

typedef struct {
    Dio_PortType PortId; /**< Numeric ID of a DIO port */
    Dio_GpioType* GpioPortPtr; /**< Pointer to the physical respective port of the channel */
    uint8 mask; /**< Mask which defines the available bits for use for a DIO port */
}Dio_PhysicalPortType;

/**
 * @}
 */

/**
 * @}
 */

/* maping arrays between ids and physical gpio entities */
extern const Dio_PhysicalChannelType DioChannelConfigData[];

extern const Dio_PhysicalChannelGroupType DioChannelGroupConfigData[];

extern const Dio_PhysicalPortType DioPortConfigData[];

extern const uint8 DioChannelConfigDataLength;

extern const uint8 DioChannelGroupConfigDataLength;

extern const uint8 DioPortConfigDataLength;

#endif
