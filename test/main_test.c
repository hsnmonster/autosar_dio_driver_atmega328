/* this file is to be compiled and burned for testing purposes */
#include "../Dio.h"
#include <util/delay.h> /* for delays */

int main () {

    /* initialization for pin 5 of port b <-> pin d13 of arduino uno */
    GPIOB->DdrRegister |= (1<<5);
    GPIOB->PortRegister &= ~(1<<5);

    while (1) {
        /* test for both channel read and write functions */
        if (Dio_ReadChannel(DIO_CHANNEL_ID_ARDUINO_UNO_LED) == STD_LOW) {
            Dio_WriteChannel(DIO_CHANNEL_ID_ARDUINO_UNO_LED,STD_HIGH);
            _delay_ms(1000);
        }
        else {
            Dio_WriteChannel(DIO_CHANNEL_ID_ARDUINO_UNO_LED,STD_LOW);
            _delay_ms(1000);
        }

    }
}

