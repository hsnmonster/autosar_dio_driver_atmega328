
/**
 ******************************************************************************
 * @file  Dio_Lcfg.c
 * @brief This is the source file (Mapping Arrays) for the link-time 
 *        configurations of the Dio module of AUTOSAR r4.3.1.
 * 
 ****************************************************************************** 
 */

#include "Dio.h"

/**
 * @addtogroup Configuration
 * @{
 */

/**
 * @addtogroup Mapping_Arrays_Symbolic_Names
 * @{
 */

/* maping arrays between ids and physical gpio entities */
const Dio_PhysicalChannelType DioChannelConfigData[] = {
    {DIO_CHANNEL_ID_ARDUINO_UNO_LED, GPIOB, 5},
    {(uint8)DIO_END_OF_LIST,(void*)0,0}
};

const Dio_PhysicalChannelGroupType DioChannelGroupConfigData[] = {
    {{0b00000011,0,DIO_PORT_ID_INPUT_BUTTONS},GPIOB},
    {{0,0,(uint8)DIO_END_OF_LIST},(void*)0}
};

const Dio_PhysicalPortType DioPortConfigData[] = {
    {DIO_PORT_ID_7_SEGMENT,GPIOD,0xFFu},
    {(uint8)DIO_END_OF_LIST,(void*)0,0x00u}
};

/**
 * @}
 */

/**
 * @}
 */

const uint8 DioChannelConfigDataLength = ((uint8)(sizeof(DioChannelConfigData)/sizeof(DioChannelConfigData[0])));

const uint8 DioChannelGroupConfigDataLength = ((uint8)(sizeof(DioChannelGroupConfigData)/sizeof(DioChannelGroupConfigData[0])));

const uint8 DioPortConfigDataLength = ((uint8)(sizeof(DioPortConfigData)/sizeof(DioPortConfigData[0])));